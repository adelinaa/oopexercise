public class Employee extends Person {
   private Company company;
   private int salary;

    public Employee(int age, String name, String CNP, Address address, boolean noOfHoursWorkedPerWeek, Company company, int salary) {
        super(age, name, CNP, address, noOfHoursWorkedPerWeek);
        this.company = company;
        this.salary = salary;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
