public class Faculty {
  private   String university;
  private   String facultyName;
  private   Country country;

    public Faculty(){}

    public Faculty(String university, String facultyName, Country country) {
        this.university = university;
        this.facultyName = facultyName;
        this.country = country;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
