public class Student extends Person {
   private Faculty faculty;
   private int yearOfStudy;

    public Student(int age, String name, String CNP, Address address, boolean noOfHoursWorkedPerWeek, Faculty faculty, int yearOfStudy) {
        super(age, name, CNP, address, noOfHoursWorkedPerWeek);
        this.faculty = faculty;
        this.yearOfStudy = yearOfStudy;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }
}
