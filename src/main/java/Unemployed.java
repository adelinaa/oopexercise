public class Unemployed extends Person {
  private   boolean spareTime;
  private   String mainJob;

    public Unemployed(int age, String name, String CNP, Address address, boolean noOfHoursWorkedPerWeek, boolean spareTime, String mainJob) {
        super(age, name, CNP, address, noOfHoursWorkedPerWeek);
        this.spareTime = spareTime;
        this.mainJob = mainJob;
    }

    public boolean isSpareTime() {
        return spareTime;
    }

    public void setSpareTime(boolean spareTime) {
        this.spareTime = spareTime;
    }

    public String getMainJob() {
        return mainJob;
    }

    public void setMainJob(String mainJob) {
        this.mainJob = mainJob;
    }
}
