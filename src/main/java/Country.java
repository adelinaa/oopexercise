public class Country {
   private String countryName;
   private String code;
   private  boolean isEU;

    public Country(){}

    public Country(String countryName, String code, boolean isEU) {
        this.countryName = countryName;
        this.code = code;
        this.isEU = isEU;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isEU() {
        return isEU;
    }

    public void setEU(boolean EU) {
        isEU = EU;
    }
}
