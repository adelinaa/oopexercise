public class Person {
    private int age;
    private String name;
    private String CNP;
    private Address address;
    boolean noOfHoursWorkedPerWeek;

    public Person(){}

    public Person(int age, String name, String CNP, Address address, boolean noOfHoursWorkedPerWeek){
        this.age = age;
        this.name = name;
        this.CNP = CNP;
        this.address = address;
        this.noOfHoursWorkedPerWeek = noOfHoursWorkedPerWeek;
    }

    public int getAge(){
        return this.age;
    }

    public void setAge(int age){
        this.age = age;
    }

    public String  getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getCNP(){
        return this.CNP;
    }

    public void setCNP(String CNP){
        this.CNP = CNP;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isNoOfHoursWorkedPerWeek() {
        return noOfHoursWorkedPerWeek;
    }

    public void setNoOfHoursWorkedPerWeek(boolean noOfHoursWorkedPerWeek) {
        this.noOfHoursWorkedPerWeek = noOfHoursWorkedPerWeek;
    }
}
