public class Main {
    public static void main(String[] args) {
        Country romania = new Country("Romania", "RO", true);

        Company SDA = new Company("SDA", romania);

        Address address = new Address(romania, "Iasi", "70074");

        Employee employee = new Employee(29, "Ion", "19111111111", address,true, SDA, 4000);

        System.out.println("The emplyee " + employee.getName() + " lives in the city: " + employee.getAddress().getCity());

        //Employee employee = new Employee(SDA, "Ion", );
    }
}
